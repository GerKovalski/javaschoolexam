package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // TODO : Implement your solution here

        return fillPyramid(inputNumbers);
    }

    public int calculateHeight(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.contains(null) || inputNumbers.size() >= Integer.MAX_VALUE - 1) {
            throw new CannotBuildPyramidException();
        }
        int height = 0;
        int tmp = 0;
        while (tmp < inputNumbers.size()) {
            tmp += height++ + 1;
        }
        if (tmp != inputNumbers.size()) {
            throw new CannotBuildPyramidException();
        }
        return height;
    }

    public int[][] fillPyramid(List<Integer> inputNumbers) {
        int height = calculateHeight(inputNumbers);
        int weight = height * 2 - 1;
        LinkedList<Integer> newInputNumbers = new LinkedList<>(inputNumbers);
        int[][] pyramid = new int[height][weight];
        newInputNumbers.sort(Comparator.naturalOrder());
        for (int i = height - 1; i > 0; i--) {
            int zeroCount = height - (i + 1);
            for (int j = 0; j < zeroCount; j++) {
                pyramid[i][j] = 0;
            }
            for (int j = weight - zeroCount - 1; j > zeroCount; j--) {
                pyramid[i][j--] = newInputNumbers.removeLast();
                pyramid[i][j] = 0;
            }
            pyramid[i][zeroCount] = newInputNumbers.removeLast();
            for (int j = weight - 1; j > weight - zeroCount - 1 ; j--) {
                pyramid[i][j] = 0;
            }

        }
        pyramid[0][height - 1] = newInputNumbers.removeLast();
        return pyramid;
    }
}
