package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    final String ALLOW_OPERATORS = "+-/*";
    final String BRACKETS = "()";
    final String DOT = ".";
    boolean badDiv = false;

    public boolean badExpression(String expression) {
        if (expression == null || expression.length() == 0) {
            return true;
        }
        int bracketCounter = 0;
        for (int i = 0; i < expression.length(); i++) {
            if (!ALLOW_OPERATORS.contains(Character.toString(expression.charAt(i)))
                    && !Character.isDigit(expression.charAt(i))
                    && !Character.toString(expression.charAt(i)).equals(DOT)
                    && !BRACKETS.contains(Character.toString(expression.charAt(i))))
            {
                return true;
            }
            if (expression.charAt(i) == '(') {
                bracketCounter++;
            }
            if (expression.charAt(i) == ')') {
                bracketCounter--;
            }
            if (bracketCounter < 0) {
                return true;
            }
            if (expression.charAt(i) == '.') {
                if (!(Character.isDigit(expression.charAt(i - 1)) && Character.isDigit(expression.charAt(i + 1)))) {
                    return true;
                }
            }
            if (ALLOW_OPERATORS.contains(Character.toString(expression.charAt(i)))) {
                if (!(expression.charAt(i + 1) == '('
                        || Character.isDigit(expression.charAt(i + 1)))) {
                    return true;
                }
            }
        }
        if (bracketCounter != 0) {
            return true;
        }
        return false;
    }

    public void execute(LinkedList<Double> linkedList, char operator) {
        double x = linkedList.removeLast();
        double y = linkedList.removeLast();
        if (operator == '+') {
            linkedList.add(x + y);
        }
        else if (operator == '-') {
            linkedList.add(y - x);
        }
        else if (operator == '*') {
            linkedList.add(x * y);
        }
        else if (operator == '/') {
            if (x != 0) {
                linkedList.add(y / x);
            }
            else badDiv = true;
        }
    }

    public int operatorsPriority(char operator) {
        if (operator == '+' || operator == '-') {
            return 1;
        }
        else if (operator == '*' || operator == '/') {
            return 2;
        }
        return 0;
    }

    public double calculate(String statement) {
        LinkedList<Character> operators = new LinkedList<>();
        LinkedList<Double> numbers = new LinkedList<>();
        for (int i = 0; i < statement.length(); i++) {
            if (statement.charAt(i) == '(') {
                operators.add('(');
            } else if (statement.charAt(i) == ')') {
                while (operators.getLast() != '(') {
                    execute(numbers, operators.removeLast());
                }
                operators.removeLast();
            }
            else if (ALLOW_OPERATORS.contains(Character.toString(statement.charAt(i)))) {
                while (!operators.isEmpty() && operatorsPriority(operators.getLast()) >= operatorsPriority(statement.charAt(i))) {
                    execute(numbers, operators.removeLast());
                }
                operators.add(statement.charAt(i));
            }
            else {
                String tmp = "";
                while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                    tmp += statement.charAt(i++);
                }
                i--;
                numbers.add(Double.parseDouble(tmp));
            }
            if (badDiv) {
                break;
            }
        }
        while (!operators.isEmpty()) {
            execute(numbers, operators.removeLast());
        }
        if (badDiv) {
            return 0;
        }
        else return numbers.removeLast();
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if (badExpression(statement)) {
            return null;
        }
        Double answer = new BigDecimal(calculate(statement)).setScale(4, RoundingMode.HALF_UP).doubleValue();
        if (badDiv) {
            return null;
        }
        if (answer % 1 == 0) {
            return String.format("%.0f", answer);
        }
        return Double.toString(answer);
    }
}
