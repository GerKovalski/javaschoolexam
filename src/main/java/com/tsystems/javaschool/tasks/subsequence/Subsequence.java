package com.tsystems.javaschool.tasks.subsequence;

import java.util.Arrays;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.isEmpty()) {
            return true;
        }
        if (y.isEmpty()) {
            return false;
        }
        int[] array = new int[x.size()];
        Arrays.fill(array, 0);
        boolean zero = false;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < y.size(); j++) {
                if (x.get(i).equals(y.get(j))) {
                    array[i] = j + 1;
                        if (array[i] == 0) {
                            zero = true;
                        }
                }
            }
        }
        int[] copyArray = new int[array.length];
        System.arraycopy(array, 0, copyArray, 0, array.length);
        Arrays.sort(copyArray);
        if (Arrays.equals(array, copyArray) && !zero) {
            return true;
        }
        return false;
    }
}
